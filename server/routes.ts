import * as express from 'express';

import CatCtrl from './controllers/cat';
import UserCtrl from './controllers/user';
import ElasticSearchCtrl from './controllers/elasticSearch';
import RoleCtrl from './controllers/role';
import Cat from './models/cat';
import User from './models/user';


export default function setRoutes(app) {

  const router = express.Router();

  const catCtrl = new CatCtrl();
  const userCtrl = new UserCtrl();
  const elasticSearchCtrl = new ElasticSearchCtrl();
  const roleCtrl = new RoleCtrl();

  // Cats
  router.route('/cats').get(catCtrl.getAll);
  router.route('/cats/count').get(catCtrl.count);
  router.route('/cat').post(catCtrl.insert);
  router.route('/cat/:id').get(catCtrl.get);
  router.route('/cat/:id').put(catCtrl.update);
  router.route('/cat/:id').delete(catCtrl.delete);

  // Roles
  router.route('/role').get(roleCtrl.getAll);
  router.route('/role/count').get(roleCtrl.count);
  router.route('/role').post(roleCtrl.insert);
  router.route('/role/:id').get(roleCtrl.get);
  router.route('/role/:id').put(roleCtrl.update);
  router.route('/role/:id').delete(roleCtrl.delete);
  

  // Users
  router.route('/login').post(userCtrl.login);
  router.route('/users').get(userCtrl.getAll);
  router.route('/users/count').get(userCtrl.count);
  router.route('/user').post(userCtrl.insert);
  router.route('/user/:id').get(userCtrl.get);
  router.route('/user/:id').put(userCtrl.update);
  router.route('/user/:id').delete(userCtrl.delete);


  // Elastic search
  router.route('/search/:message').get(elasticSearchCtrl.ping)




  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}
