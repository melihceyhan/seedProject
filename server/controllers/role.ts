import Role from '../models/role';
import BaseCtrl from './base';

export default class RoleCtrl extends BaseCtrl {
  model = Role;
}
