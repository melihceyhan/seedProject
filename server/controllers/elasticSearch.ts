import * as elasticsearch from 'elasticsearch';

export default class ElasticSearchCtrl
{
    private client: elasticsearch.Client
    
     private connect() {
       this.client = new elasticsearch.Client({
         host: 'localhost:9200',
         log: 'trace'
       });
     }

     ping = (req, res) => {
       this.connect();
      this.client.ping({
        requestTimeout: Infinity,
        body: req.params.message
      });
      res.json(req.params.message);
     
    }
     
}
 