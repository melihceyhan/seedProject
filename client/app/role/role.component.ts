import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { RoleService } from '../services/role.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-roles',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  role = {};
  roles = [];
  isLoading = true;
  isEditing = false;

  addRoleForm: FormGroup;
  name = new FormControl('', Validators.required);

  constructor(private roleService: RoleService,
              private formBuilder: FormBuilder,
              public toast: ToastComponent) { }

  ngOnInit() {
    this.getRoles();
    this.addRoleForm = this.formBuilder.group({
      name: this.name,
    });
  }

  getRoles() {
    this.roleService.getRoles().subscribe(
      data => this.roles = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  addRole() {
    this.roleService.addRole(this.addRoleForm.value).subscribe(
      res => {
        const newRole = res.json();
        this.roles.push(newRole);
        this.addRoleForm.reset();
        this.toast.setMessage('item added successfully.', 'success');
      },
      error => console.log(error)
    );
  }

  enableEditing(role) {
    this.isEditing = true;
    this.role = role;
  }

  cancelEditing() {
    this.isEditing = false;
    this.role = {};
    this.toast.setMessage('item editing cancelled.', 'warning');
   
    this.getRoles();
  }

  editRole(role) {
    this.roleService.editRole(role).subscribe(
      res => {
        this.isEditing = false;
        this.role = role;
        this.toast.setMessage('item edited successfully.', 'success');
      },
      error => console.log(error)
    );
  }

  deleteRole(role) {
    if (window.confirm('Are you sure you want to permanently delete this item?')) {
      this.roleService.deleteRole(role).subscribe(
        res => {
          const pos = this.roles.map(elem => elem._id).indexOf(role._id);
          this.roles.splice(pos, 1);
          this.toast.setMessage('item deleted successfully.', 'success');
        },
        error => console.log(error)
      );
    }
  }

}
