import { Component, OnInit } from '@angular/core';
import { ElasticService } from '../services/elastic.service';
@Component({
  selector: 'app-elastic',
  templateUrl: './elastic.component.html',
  styleUrls: ['./elastic.component.scss']
})
export class ElasticComponent implements OnInit {

  constructor(private elasticService: ElasticService,) { }

  ngOnInit() {
  }
  
  
  ping() {
    this.elasticService.ping().subscribe(
      data => console.log(data),
      error => console.log(error),
    
    );
  }
}
