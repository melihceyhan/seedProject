import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';

import  Role  from '../../../server/models/role';

import { UserService } from '../services/user.service';
import { RoleService } from '../services/role.service';

@Injectable()
export class AuthService {
  loggedIn = false;
  isAdmin = false;
  
  jwtHelper: JwtHelper = new JwtHelper();

  currentUser = { _id: '', username: '', roleName: '' };

  constructor(private userService: UserService, private roleService : RoleService,
              private router: Router) {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedUser = this.decodeUserFromToken(token);
      this.setCurrentUser(decodedUser);
    }
  }

  login(emailAndPassword) {
    return this.userService.login(emailAndPassword).map(res => res.json()).map(
      res => {
        localStorage.setItem('token', res.token);
        const decodedUser = this.decodeUserFromToken(res.token);
       
        this.setCurrentUser(decodedUser);
        return this.loggedIn;
      }
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.loggedIn = false;
    this.isAdmin = false;
    this.currentUser = { _id: '', username: '', roleName: '' };
    this.router.navigate(['/']);
  }

  decodeUserFromToken(token) {
    return this.jwtHelper.decodeToken(token).user;
  }

  setCurrentUser(decodedUser) {
    this.loggedIn = true;
    this.currentUser._id = decodedUser._id;
    this.currentUser.username = decodedUser.username;
    this.currentUser.roleName = decodedUser.role.name;
    this.currentUser.roleName === 'Admin' ? this.isAdmin = true : this.isAdmin = false;
    console.log(this.currentUser.roleName);
    console.log(this.isAdmin);
  }

}
