import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class RoleService {

  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) { }

  getRoles(): Observable<any> {
    return this.http.get('/api/role').map(res => res.json());
  }

  countRoles(): Observable<any> {
    return this.http.get('/api/role/count').map(res => res.json());
  }

  addRole(role): Observable<any> {
    return this.http.post('/api/role', JSON.stringify(role), this.options);
  }

  getRole(role): Observable<any> {
    return this.http.get(`/api/role/${role._id}`).map(res => res.json());
  }

  getRoleById(roleId): Observable<any> {
    return this.http.get(`/api/role/${roleId}`).map(res => res.json());
  }


  editRole(role): Observable<any> {
    return this.http.put(`/api/role/${role._id}`, JSON.stringify(role), this.options);
  }

  deleteRole(role): Observable<any> {
    return this.http.delete(`/api/role/${role._id}`, this.options);
  }

}
