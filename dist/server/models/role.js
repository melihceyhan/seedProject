"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var roleSchema = new mongoose.Schema({
    name: { type: String, required: true }
});
var Role = mongoose.model('Role', roleSchema);
exports.default = Role;
//# sourceMappingURL=role.js.map