"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var elasticsearch = require("elasticsearch");
var ElasticSearchCtrl = /** @class */ (function () {
    function ElasticSearchCtrl() {
        var _this = this;
        this.ping = function (req, res) {
            _this.connect();
            _this.client.ping({
                requestTimeout: Infinity,
                body: req.params.message
            });
            res.json(req.params.message);
        };
    }
    ElasticSearchCtrl.prototype.connect = function () {
        this.client = new elasticsearch.Client({
            host: 'localhost:9200',
            log: 'trace'
        });
    };
    return ElasticSearchCtrl;
}());
exports.default = ElasticSearchCtrl;
//# sourceMappingURL=elasticSearch.js.map