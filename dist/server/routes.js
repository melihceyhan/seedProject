"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cat_1 = require("./controllers/cat");
var user_1 = require("./controllers/user");
var elasticSearch_1 = require("./controllers/elasticSearch");
var role_1 = require("./controllers/role");
function setRoutes(app) {
    var router = express.Router();
    var catCtrl = new cat_1.default();
    var userCtrl = new user_1.default();
    var elasticSearchCtrl = new elasticSearch_1.default();
    var roleCtrl = new role_1.default();
    // Cats
    router.route('/cats').get(catCtrl.getAll);
    router.route('/cats/count').get(catCtrl.count);
    router.route('/cat').post(catCtrl.insert);
    router.route('/cat/:id').get(catCtrl.get);
    router.route('/cat/:id').put(catCtrl.update);
    router.route('/cat/:id').delete(catCtrl.delete);
    // Roles
    router.route('/role').get(roleCtrl.getAll);
    router.route('/role/count').get(roleCtrl.count);
    router.route('/role').post(roleCtrl.insert);
    router.route('/role/:id').get(roleCtrl.get);
    router.route('/role/:id').put(roleCtrl.update);
    router.route('/role/:id').delete(roleCtrl.delete);
    // Users
    router.route('/login').post(userCtrl.login);
    router.route('/users').get(userCtrl.getAll);
    router.route('/users/count').get(userCtrl.count);
    router.route('/user').post(userCtrl.insert);
    router.route('/user/:id').get(userCtrl.get);
    router.route('/user/:id').put(userCtrl.update);
    router.route('/user/:id').delete(userCtrl.delete);
    // Elastic search
    router.route('/search/:message').get(elasticSearchCtrl.ping);
    // Apply the routes to our application with the prefix /api
    app.use('/api', router);
}
exports.default = setRoutes;
//# sourceMappingURL=routes.js.map